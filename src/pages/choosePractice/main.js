/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-04 09:59:32
 * @LastEditTime: 2019-09-04 10:00:30
 * @LastEditors: Please set LastEditors
 */
import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()